const mongoose = require('mongoose')

const Todo = mongoose.Schema({
    title: { type: String, required: true},
    description: { type: String, required: true},
    isCompleted: { type: String, required: true, default: false},
    createdAt: { type: Date, default: Date.now},
})

module.exports = mongoose.model('todo', Todo, 'todos')