const express = require('express')
const rateLimit = require('express-rate-limit');
const { createTodoValidation } = require('../../validations/todos-validations');
const router = express.Router()
const limiter = rateLimit({
    windowMs: 5 * 60 * 1000, // 5 minutes
    max: 10 // limit each IP to 1 requests per windowMs
});
router.get('/', (req, res) => {
    res.status(200).json({ message: 'Hello world' })
})

router.post('/', limiter, async (req, res, next) => {
try {
    const isValidTodo = createTodoValidation.validate(req.body)
    if(!isValidTodo) return res.status(400)
    const newTodo = {
        title: req.body.title.trim(),
        descrption: req.body.descrption.trim(),
    }
    res.status(201).json(newTodo)
} catch (error) {
    await next(error)
}
})
module.exports = router