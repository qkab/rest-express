const express = require('express')
const rateLimit = require('express-rate-limit')
const todos = require('./todos-route')
const router = express.Router()
router.use('/todos', todos)
router.get('/', (req, res) => {
    res.status(200).json({ message: 'API Version 1 - 🌍' })
})
module.exports = router