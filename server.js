const express = require('express')
const helmet = require('helmet')
const cors = require('cors')
const api = require('./api/routes')
const app = express()

app.use(helmet())
app.use(cors())
app.use(express.json())


app.use('/api/v1', api)
let port = process.env.PORT || 5000
app.listen(port, () => console.log(`Listening on port ${port}`))