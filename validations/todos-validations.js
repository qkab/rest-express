const joi = require('@hapi/joi')
const Joi = require('@hapi/joi')

const createTodoValidation = Joi.object({
    title: Joi.string().trim().required(),
    description: Joi.string().trim().required()
})

module.exports = { createTodoValidation }